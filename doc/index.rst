ewokstxs |version|
===================

*ewokstxs* provides Ewoks tasks for the [txs package](https://gitlab.esrf.fr/levantin/txs).

*ewokstxs* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
and the ID09 staff of the `European Synchrotron <https://www.esrf.eu/>`_.

Documentation
-------------

.. toctree::
    :maxdepth: 2

    api
