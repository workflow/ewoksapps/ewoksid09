from __future__ import annotations

import logging
import os.path
from typing import Generator

from blissdata.beacon.data import BeaconData
from blissdata.redis_engine.exceptions import ScanNotFoundError, ScanValidationError
from blissdata.redis_engine.store import DataStore

from ewoksdata.data import nexus
from ewoksdata.data.bliss import iter_bliss_scan_data, iter_bliss_scan_data_from_memory
from ewoksdata.data.hdf5.dataset_writer import DatasetWriter

import h5py

import numpy as np

from pyFAI.containers import Integrate1dResult

from silx.io import h5py_utils
from silx.io.dictdump import dicttonx
from silx.io.url import DataUrl

import txs


_logger = logging.getLogger(__name__)


RETRY_OPTIONS = {"retry_timeout": 2 * 60, "retry_period": 1}
"""Retry options used both for reading and writing"""


@h5py_utils.retry()
def _open_h5_file(filename: str, mode: str = "r"):
    """Open a HDF5 file and retries if it fails"""
    return h5py_utils.File(filename, mode)


class TxsResultsWriter:
    """Writes txs results to a HDF5 group.

    :param scan_nxentry_url: URL of the HDF5 group of the scan
    :param results_nxentry_url: URL of the HDF5 group where to write the results
    :param nxprocess_group_name: Name of the subgroup storing the txs results
    :param nxdata_group_name: Name of the subgroup of the nxprocess_group storing the plot
    """

    FLUSH_PERIOD = 5
    """Period at which to flush the HDF5 file (seconds)"""

    def __init__(
        self,
        scan_nxentry_url: DataUrl,
        results_nxentry_url: DataUrl,
        nxprocess_group_name: str,
        nxdata_group_name: str,
    ):
        # Create the output result group and create folders and HDF5 file if needed
        nexus.create_url(results_nxentry_url, **RETRY_OPTIONS)

        self._h5file = _open_h5_file(
            results_nxentry_url.file_path(), mode="a", **RETRY_OPTIONS
        )

        # Use a relative path for the external links
        scan_filename = os.path.relpath(
            scan_nxentry_url.file_path(),
            os.path.dirname(results_nxentry_url.file_path()),
        )
        scan_group_name = scan_nxentry_url.data_path()

        dicttonx(
            {
                # Add links to some the scan entries
                "instrument": h5py.ExternalLink(
                    scan_filename, f"{scan_group_name}/instrument"
                ),
                "measurement": h5py.ExternalLink(
                    scan_filename, f"{scan_group_name}/measurement"
                ),
                "sample": h5py.ExternalLink(scan_filename, f"{scan_group_name}/sample"),
                "title": h5py.ExternalLink(scan_filename, f"{scan_group_name}/title"),
                # Add NXprocess subgroup storing txs results
                "@default": nxprocess_group_name,
                nxprocess_group_name: {
                    "@NX_class": "NXprocess",
                    "@default": "integrated",
                    nxdata_group_name: {
                        "@NX_class": "NXdata",
                        # Group where to store results data
                    },
                    "configuration": {
                        "@NX_class": "NXcollection",
                        # TODO
                    },
                    "program": "txs",
                    "version": txs.__version__,
                },
            },
            self._h5file,
            results_nxentry_url.data_path(),
            update_mode="modify",
        )
        self._h5file.flush()

        results_group = self._h5file[results_nxentry_url.data_path()]
        self._nxdata_group = results_group[
            f"{nxprocess_group_name}/{nxdata_group_name}"
        ]
        self._nxdata_url = DataUrl(
            file_path=self._nxdata_group.file.filename,
            data_path=self._nxdata_group.name,
        )
        self._nxdata_init = False

        # Create the data writers
        self._intensity_writer = DatasetWriter(
            self._nxdata_group,
            "intensity",
            flush_period=self.FLUSH_PERIOD,
        )
        self._error_writer = DatasetWriter(
            self._nxdata_group,
            "intensity_errors",
            flush_period=self.FLUSH_PERIOD,
        )

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    @property
    def closed(self) -> bool:
        return not bool(self._h5file)  # h5py's way to check for closed file

    @property
    def nxdata_url(self) -> DataUrl:
        """URL of the HDF5 group storing the NXdata"""
        return self._nxdata_url

    def _init_from_result(self, result: Integrate1dResult):
        radial_name, radial_unit = result.unit.name.split("_")
        dicttonx(
            {
                "@NX_class": "NXdata",
                "@signal": "intensity",
                "@axes": [".", radial_name],
                "@interpretation": "spectrum",
                radial_name: result.radial,
                f"{radial_name}@units": radial_unit,
            },
            self._nxdata_group.file,
            self._nxdata_group.name,
            update_mode="modify",
        )

    def add_result(self, result: Integrate1dResult):
        """Append data from txs result to the HDF5 datasets"""
        if not self._nxdata_init:
            # Populate NXdata group from first result
            self._init_from_result(result)
            self._nxdata_init = True

        # Append intensity and error
        flush = self._intensity_writer.add_point(result.intensity)
        if result.sigma is not None:
            flush |= self._error_writer.add_point(result.sigma)
        if flush:
            self._nxdata_group.file.flush()

    def close(self):
        """Close the HDF5 file where results are saved."""
        if self.closed:
            return

        # Make sure all data is written
        self._intensity_writer.flush_buffer()
        self._error_writer.flush_buffer()
        self._h5file.close()


def detector_frames(
    scan_key: str | None,
    filename: str | None,
    scan_number: int,
    detector: str,
) -> Generator[np.ndarray]:
    """Generator of detector frames from a scan retrieved either through redis or from file

    At least one of scan_key and filename must not be None or empty.

    :scan_key: blissdata key of the scan
    :filename: Path of the HDF5 raw data file to read
    :scan_entry_name: HDF5 path in the file of the group containing the scan data
    :detector: name of the detector
    """
    if not scan_key and not filename:
        raise ValueError("At least one of scan_key and filename must not None or empty")

    if scan_key:  # Try using redis first
        # TODO rework
        try:
            data_store = DataStore(BeaconData().get_redis_data_db())
        except BaseException:
            _logger.info("Cannot connect to beacon host or redis")
            _logger.debug("Backtrace", exc_info=True)
        else:
            try:
                _ = data_store.load_scan(scan_key)
            except (ScanNotFoundError, ScanValidationError):
                _logger.info(f"Cannot retrieve scan from redis: {scan_key}")
                _logger.debug("Backtrace", exc_info=True)
            else:
                _logger.info("Retrieve frames through redis")
                for data in iter_bliss_scan_data_from_memory(
                    scan_key, lima_names=[detector], counter_names=[], **RETRY_OPTIONS
                ):
                    yield data[detector]
                return

    if not filename:
        raise RuntimeError("Cannot connect to redis or retrieve the scan")

    _logger.info("Read frames from file")
    for data in iter_bliss_scan_data(
        filename,
        scan_number,
        lima_names=[detector],
        # One counter is needed here to make sure to iterate over the right number of frames
        counter_names=["elapsed_time"],
        **RETRY_OPTIONS,
    ):
        _logger.info(f"Read image for {data['elapsed_time']}")
        yield data[detector]
