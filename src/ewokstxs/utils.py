TXS_INPUTS_KEYS = (
    "dezinger_method",
    "dezinger",
    "sensor_material",
    "sensor_thickness",
    "sensor_density",
    "sample_material",
    "sample_thickness",
    "sample_density",
)

PROCESS_KEYS = (
    "npt",
    "method",
    "unit",
    "mask",
    "dark",
    "flat",
    "solid_angle",
    "polarization_factor",
    "error_model",
    "radial_range",
    "dummy",
    "delta_dummy",
    "safe",
    *TXS_INPUTS_KEYS,
)
