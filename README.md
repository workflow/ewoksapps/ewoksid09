# ewokstxs

Ewoks tasks for the [txs package](https://gitlab.esrf.fr/levantin/txs).

## Documentation

https://ewokstxs.readthedocs.io/
